<?php

class Colsort_IndexController extends Omeka_Controller_AbstractActionController
{

  protected $tree = '';

  public function affichecollectionsAction() {
    $db = get_db();
    // Nombre total d'items ayant une collection
    $private = '';
    if (! current_user()) {
      $private = 'AND public = 1';
    }
    $nbItems = $db->query("SELECT COUNT(id) nb FROM `$db->Items` WHERE collection_id <> 0 $private")->fetchObject();
    $this->view->nbItems = $nbItems->nb;
    $query = "SELECT collection_id id, name, c.public public FROM omeka_collection_trees t LEFT JOIN omeka_collections c ON t.collection_id = c.id WHERE t.parent_collection_id = 0";
    $cols = $db->query($query)->fetchAll();
    $this->tree .= '<ul>';
    $cols = $this->orderCollections($cols);
    foreach ($cols as $id => $col) {
      if ($col['public'] <> 1 && ! current_user()) {continue;}
      $collection = get_record_by_id('collection', $col['id']);
      $this->tree .= "<li><a class='collection' href='collections/show/" . $col['id'] . "' data='" . $col['id'] . "'>" . $col['name'] . "</a>";
      $plus = '';
      $child_collections = $this->fetch_child_collections($col['id']);
      $notices = $db->query("SELECT 1 FROM `$db->Items` WHERE collection_id = " . $col['id'])->fetchObject();
      $plus = '';
      if ($child_collections || $notices) {
        $plus = "<span class='montrer'> +</span>";
      }
      $this->tree .= $plus . $child_collections . "</li>";
    }
    $this->tree .= '</ul>';
    $this->view->tree = $this->tree;
    return true;
  }

  private function fetch_child_collections($collection_id) {
    $children = '';
    $db = get_db();
    $query = "SELECT t.collection_id id, name, c.public public FROM `$db->CollectionTrees` t INNER JOIN `$db->Collections` c ON t.collection_id = c.id WHERE parent_collection_id = " . $collection_id . " ORDER BY name";
    $child_collections = $db->query($query)->fetchAll();
    if (! $child_collections) return false;
    $children .= '<div class="collections hidden"><ul>';
    $child_collections = $this->orderCollections($child_collections);
    $plus = '';
    foreach ($child_collections as $id => $col) {
      if ($col['public'] <> 1 && ! current_user()) {continue;}
      $collection = get_record_by_id('collection', $col['id']);
      $plus = "<span class='montrer'> +</span>";
      $child_collections = $this->fetch_child_collections($col['id']);
      $notices = $db->query("SELECT 1 FROM `$db->Items` WHERE collection_id = " . $col['id'])->fetchObject();
      $plus = '';
      if ($child_collections || $notices) {
        $plus = "<span class='montrer'> +</span>";
      }
      $children .= "<li><a class='collection' href='collections/show/". $col['id'] . "' data='" . $col['id'] . "'>" . $col['name'] . " </a> $plus $child_collections</li>";
    }
    $children .= '</ul></div>';
    return $children;
  }

  public function ajaxFetchItemsAction() {
    if (! $cid = $this->getParam('cid')) {
      $this->_helper->json('');
    };
    $items = $this->fetch_items($cid);
    $this->_helper->json($items);
  }

  private function fetch_items($cid) {
    $notices = "";
    $db = get_db();

    $items = $db->query("SELECT DISTINCT(i.id) id FROM `$db->Items` i LEFT JOIN `$db->ElementTexts` e ON i.id = e.record_id WHERE e.element_id = '50' AND e.record_type = 'Item' AND collection_id = " . $cid)->fetchAll();

    if (! $items) {return false;}

    // Sort items by item order module
    $ordre = $db->query("SELECT item_id, o.order ordre FROM `$db->ItemOrderItemOrders` o WHERE collection_id = $cid")->fetchAll();
    if ($ordre) {
      $order = array();
      foreach ($ordre as $i => $vals) {
        $order[$vals['item_id']] = $vals['ordre'];
      }
    }

    foreach ($items as $id => $item) {
      if (isset($item['id']) && isset($order[$item['id']])) {
        $items[$id]['ordre'] = $order[$item['id']];
      }
      $record = get_record_by_id('Item', $item['id']);
      if ($record) {
        $items[$id]['title'] = metadata($record, array('Dublin Core', 'Title'));
      }
    }

    $sansordre = array_filter($items, function($a) { if (! isset($a['ordre'])) {return true;} else {return false;} });
    $avecordre = array_filter($items, function($a) { if (isset($a['ordre'])) {return true;} else {return false;} });

    usort($avecordre, function($a, $b) {if (!isset($a['ordre']) || !isset($b['ordre'])) {return 1;}; return ($a['ordre'] < $b['ordre']) ? -1 : 1;});

    if (get_option('sortcol_tri')) {
      $sansordre = eman_sort_array($sansordre, 'value_list', 'title');
    } else {
      usort($sansordre, function($a, $b) {return $a['id'] < $b['id'] ? -1 : 1;});
    }

    $items = $avecordre;
    foreach ($sansordre as $i => $val) {
      $items[] = $val;
    }

    $notices .= '<div class="notices"><ul>';
    foreach ($items as $id => $item) {
      $item = get_record_by_id('item', $item['id']);
      if ($item) {
       $notices .= "<li style='list-style-type:circle;'><a href='items/show/" . metadata($item, 'id') . "' >" . metadata($item, array('Dublin Core', 'Title')). "</a></li>";
      }
    }
    $notices .= '</ul></div>';
    return $notices;
  }

   public function orderCollections($cols) {
    $order = unserialize(get_option('sortcol_preferences'));
    foreach ($cols as $id => $col) {
      if (isset($order[$col['id']])) {
        if ($order[$col['id']] <> "") {
          $cols[$id]['ordre'] = $order[$col['id']];
        } else {
          $cols[$id]['ordre'] = 0;
        }
      } else {
        $cols[$id]['ordre'] = 0;
      }
    }
    $sansnumero = array_filter($cols, function($a) { if ($a['ordre'] == 0) {return true;} else {return false;} });
    $avecnumero = array_filter($cols, function($a) { if ($a['ordre'] <> 0) {return true;} else {return false;} });

    setlocale(LC_COLLATE, 'fr_FR.utf8');
    usort($avecnumero, function($a, $b) {return $a['ordre'] > $b['ordre'];});
    $sansnumero = eman_sort_array($sansnumero, 'value_list', 'name');

    $cols = $sansnumero;
    foreach ($avecnumero as $i => $val) {
      $cols[] = $val;
    }
    return $cols;
  }

}