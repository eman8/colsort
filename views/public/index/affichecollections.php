<?php
queue_js_file('colsort');
echo head(array('title' => 'Arborescence du corpus', 'bodyclass' => 'collections browse')); ?>

<style>
.montrer {
  cursor:pointer;
  color:#aaa;
}
.collection {
  font-weight : bold;
}
#titre-arbo {
  font-weight: bold;
}
.tout {
  cursor:pointer;
}
.hidden {
  display:none;
}
</style>

<div id='collection-tree'>
<h3 id="titre-arbo">Arborescence du corpus</h3>
<span style="float:right;clear:both;" class='tout' data='<?php echo $nbItems ?>'>Tout d&eacute;plier</span><br />
<?php  echo $tree; ?>
</div>
<?php echo foot(); ?>