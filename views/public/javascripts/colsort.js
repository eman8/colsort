window.jQuery = window.$ = jQuery;

jQuery(document).ready(function() {
  $('.montrer').click(function(e) {
    listElement = $(this).parent();
    if ($(this).text() == ' +') {
      listElement.children('.collections').removeClass('hidden');
      if (listElement.children('.notices').length == 0) {
        var element = $(this).parent();
        path = window.location.pathname;
        cid = $(listElement).find('.collection').first().attr('data');
        $('body').addClass('waiting');
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: path.substr(0, path.lastIndexOf("/")) + '/colsort/ajaxfetchitems',
          data: { cid: cid },
          async: true,
          success: function(json) {
            element.append(json);
          }
        });
        $('body').removeClass('waiting');
      }
      listElement.children('.notices').removeClass('hidden');
      $(this).text(' -');
    } else {
      listElement.children('.collections, .notices').addClass('hidden');
      $(this).text(' +');
    }
  });
  $('.tout').click(function() {
    if ($(this).html() == 'Tout replier') {
      $(this).html('Tout d&eacute;plier');
    } else {
      nb = $(this).attr('data');
      if (nb > 100) {
        if (! confirm('Montrer la totalité des contenus, dont les ' + nb + ' items du corpus ?')) {
          return false;
        }
      }
      $(this).html('Tout replier');
    }
    $('body').addClass('waiting');
    $('.montrer').trigger('click');
    $('body').removeClass('waiting');
  });
});